

$('.tabs-title').click(function () {
   if (!$(this).hasClass('active')){
       let contentId = $(this).attr('data-filter');

       $('.tabs-title.active').removeClass('active');
       $('.content.active').removeClass('active');
       $(this).addClass('active');
       $('#'+contentId).addClass('active');
   }
});


